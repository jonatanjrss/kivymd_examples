from kivy.app import App
from kivy.lang import Builder

from kivymd.theming import ThemeManager


class NavBarApp(App):
    theme_cls = ThemeManager()


NavBarApp().run()
