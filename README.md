# Exemplo de Barra de Navegação

Na linha 10, você pode alterar o valor do atributo "title".

Na linha 12, você pode alterar o valor do atributo "icon".

Na linha 13, você pode alterar o valor do atributo "text".

Na linha 14, você pode alterar o valor de "app.root.ids.scr_mngr.current", mas observe que você também terá de alterar o valor do atributo "name" na linha 31.

Na linha 16, você pode alterar o valor do atributo "icon".

Na linha 17, você pode alterar o valor do atributo "text".

Na linha 18, você pode alterar o valor de "app.root.ids.scr_mngr.current", mas observe que você também terá de alterar o valor do atributo "name" na linha 33.

Na linha 23, você pode alterar o valor do atributo "title".

Na linha 27 você pode alterar a string 'menu', confira outras opções de ícones nesse site https://zavoloklom.github.io/material-design-iconic-font/icons.html (não precisa baixar nada, é só substituir a palavra 'menu' pelo nome do ícone que desejar).

Na linha 31, você pode alterar o valor do atributo "name", mas observe que você também terá de alterar o valor de "app.root.ids.scr_mngr.current" na linha 14.

Na linha 33, você pode alterar o valor do atributo "name", mas observe que você também terá de alterar o valor de "app.root.ids.scr_mngr.current" na linha 18.


OBS.: Além desses, outros atributos podem ser alterados. Citei apenas os mais básicos.
